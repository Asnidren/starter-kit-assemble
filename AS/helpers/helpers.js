var merge = require('mixin-deep');
module.exports.register = function (Handlebars, options, params)  { 
	Handlebars.registerHelper('renderpartial', function(name, ctx, hash) {
		var fn;
		var template = Handlebars.partials[name];

		if (typeof template !== 'Function' ){
			fn = Handlebars.compile(template);
		}else{
			fn = template;
		}
		//console.log(ctx);
		var output = fn(merge(this, ctx)).replace(/^\s+/, '');

		return new Handlebars.SafeString(output);
	});
	Handlebars.registerHelper('safeVal', function(value, safeValue){
		var out = value || safeValue;
		return new Handlebars.SafeString(out);
	});
};
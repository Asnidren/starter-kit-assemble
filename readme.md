# Starter-Kit assemble

## Installation


* Copiez ou récupérez le contenu du dossier dans votre dossier projet.
* Dans la console, placez vous dans votre dossier projet, lancez:

```
#!javascript

$npm install
$bower install
```

* Génération des sorties HTML :


```
#!javascript

$grunt assemble
```



* Activation du localhost (pour visualiser correctement l'API en angular) :


```
#!javascript

$grunt localhost
```





Les pages générées sont disponibles sur :

* http://localhost:9002/OUT-WWW/

L'API générée est disponible sur :

* http://localhost:9002/OUT-API/index.html



## Ajouter un composant (*ex: slider de page d'accueil*)

* Dans /AS/hbs/partials/..., créer le template du composant (*ex: slider.hbs*)

PAGES:

* Dans /AS/hbs/bodies/pages/..., ajouter les inclusions du nouveau template, et/ou dans /AS/data/pages/..., ajouter les appels dynamiques du nouveau composant
* Dans /AS/data/components/, créer un dossier de fichiers *.yml déclinant les variables du composant (*ex: slider/slider_A.yml, slider/slider_B.yml*)

API:

* Dans /AS/hbs/bodies/api-components/..., créer autant de fichiers qu'il y a de déclinaisons (*ex: slider/slider_A.tpl.hbs, slider/slider_B.tpl.hbs*)
* Dans /AS/data/api-objects.json, ajouter l'objet représentant le composant, indiquer les noms, les pages finales des variantes (sans le .tpl.html), les descriptions d'API

* Lancer 

```
#!javascript

$grunt assemble
```



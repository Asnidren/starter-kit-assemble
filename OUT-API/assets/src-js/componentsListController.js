(function(){
    'use strict';

    function ComponentsListController($log,$scope,ComponentService){
    	var self = this;
    	var promise = ComponentService.findAll('components');
      promise.then(function(obj) {
          $scope.components = obj;
      }, function(reason) {});
      promise = ComponentService.findAll('integrations');
      promise.then(function(obj) {
          $scope.integrations = obj;
      }, function(reason) {});  


    }



    angular.module('app')
        .controller('ComponentsListController',ComponentsListController);
})();
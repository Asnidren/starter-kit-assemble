(function(){
    'use strict';

    function ComponentController($log,folder,component){
    	var self = this;
    	self.component = component;

        self.template = function(variation){
            return '/OUT-API/tpl-api/'+folder+'/'+variation.template+'.tpl.html';
        };

    }



    angular.module('app')
        .controller('ComponentController',ComponentController);
})();
(function(){
    'use strict';

    function ComponentService($log,$http,$q){

        function getObjectOfID(list,itemId){
            if(list==undefined){
                $log.warn('[getObjectOfID] cannot get item from the list, list is undefined, do nothing.');
            }else if(itemId==undefined){
                $log.warn('[getObjectOfID] cannot get item from the list, itemId is undefined, do nothing.');
            }else if(list.length==0){
                //$log.debug('[getObjectOfID] cannot get item from the list, list is empty, do nothing.');
            }else{
                for (var i = 0; i < list.length; i++){
                    //$log.debug('[getObjectOfID] ' + list[i].id + ' vs ' + itemId);
                    if(list[i].id == itemId){
                        return list[i];
                    }
                }
            }
            return undefined;
        }

        var file = '/AS/data/api-objects.json';

        return {
            findOne: function(folder,id){
                return $q(function(resolve,reject){
                    var path = file;
                    $log.debug('Search for component of id '+id+' of ' + folder + ' in file "' + path + '"...');
                    // charger le json de nom id
                    $http.get(path).
                      success(function(data, status, headers, config) {
                        var filecontent = angular.fromJson(data);
                        //$log.debug(filecontent);
                        var objectInJson = getObjectOfID(filecontent[folder], id);
                        $log.debug(objectInJson);
                        resolve(objectInJson);
                      }).
                      error(function(err) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                        $log.log(' findOneerror');
                        reject(err);
                      });
                });
                
            },
            findAll: function(folder){
                return $q(function(resolve,reject){
                    var path = file;
                    $log.debug('Search for components list of ' + folder + ' in file "' + path + '"...');
                    $http.get(path).
                      success(function(data, status, headers, config) {
                        var filecontent = angular.fromJson(data);
                        $log.debug(filecontent[folder]);
                        resolve(filecontent[folder]);
                      }).
                      error(function(err) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                        $log.log('findAll error');
                        reject(err);
                      });
                });
                
            }

        };
    }

    angular.module('app')
        .factory('ComponentService',ComponentService);
})();
(function(){
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'mm.foundation'
        ])
        .config(function($stateProvider,$urlRouterProvider){
        	$stateProvider
                .state('home',{
                    url: '/home',
                    views: {
                        global: {
                            controller: 'HomeController as home',
                            templateUrl: 'tpl/home.tpl.html'
                        }
                    }
                })
                .state('component',{
                    url: '/component/:id',
                    resolve: {
                        component: function(ComponentService, $stateParams, $log){
                            console.log('components ' + $stateParams.id);
                            var result = ComponentService.findOne('components',$stateParams.id);
                            return result;
                        },
                        folder: function(){
                            return 'components';
                        }
                    },
                    views: {
                        global: {
                            controller: 'ComponentController as c',
                            templateUrl: 'tpl/component.tpl.html'
                        }
                    }
                })
                .state('integration',{
                    url: '/integration/:id',
                    resolve: {
                        component: function(ComponentService, $stateParams, $log){
                            console.log('integrations ' + $stateParams.id);
                            var result = ComponentService.findOne('integrations',$stateParams.id);
                            return result;
                        },
                        folder: function(){
                            return 'integrations';
                        }
                    },
                    views: {
                        global: {
                            controller: 'ComponentController as c',
                            templateUrl: 'tpl/component.tpl.html'
                        }
                    }
                });
            $urlRouterProvider.otherwise('/home');
        });
        
})();
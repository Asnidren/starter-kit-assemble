var path = require('path');
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;
var folderMount = function folderMount(connect, point) {
  return connect.static(path.resolve(point));
};

module.exports = function(grunt) {

  var cfg= {
    connectPort: 9002,
    sassOutputStyle: 'compact'
  };

	grunt.initConfig({
    cfg:cfg,
    pkg: grunt.file.readJSON('package.json'),
    
    assemble: {
      options:{ 
        flatten: true,
        data: 'AS/data/**/**/*.yml'
      },
      www: {
        options: {
          layout: 'AS/hbs/layouts/page.hbs',
          partials: 'AS/hbs/partials/**/*.hbs',
          helpers: 'AS/helpers/**/*.js',
          data: 'AS/data/**/**/*.yml'
        },
        files: {
          'OUT-WWW/': ['AS/hbs/bodies/pages/*.hbs']
        }
      },
      apicomponents: {
        options: {
          layout: 'AS/hbs/layouts/component.hbs',
          partials: 'AS/hbs/partials/**/*.hbs',
          helpers: 'AS/helpers/**/*.js',
          data: 'AS/data/**/**/*.yml'
        },
        files: {
          'OUT-API/tpl-api/components/': ['AS/hbs/bodies/api-components/*.hbs'],
          'OUT-API/tpl-api/integrations/': ['AS/hbs/bodies/api-integration/*.hbs']
        }
      }
    },

    connect: {
      livereload: {
        options: {
          port:  cfg.connectPort,
          keepalive: true,          
          middleware: function(connect, options) {
            return [lrSnippet, folderMount(connect, '.')]
          }
        }
      },
      'livereload-start' :{
        options: {
          port: 9003
        }
      }
    }

  });

	grunt.loadNpmTasks('assemble' );
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-livereload');

  grunt.registerTask('localhost', ['livereload-start','connect']);
}